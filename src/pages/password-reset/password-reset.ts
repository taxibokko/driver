import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PasswordResetComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@IonicPage()
@Component({
  selector: 'password-reset',
  templateUrl: 'password-reset.html'
})
export class PasswordReset {


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  
  }

  backButtonClick(){
    this.navCtrl.pop();
  }



}
