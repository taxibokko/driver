import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class Checkout {

  selectImg = 2;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

// select method payment
  activeSelect(index){
    this.selectImg = index;
  }

}