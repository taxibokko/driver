import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import $ from "jquery";
import 'intl-tel-input';
import { RestProvider } from '../../providers/rest/rest';
import { DriverEntity } from '../../entities/driverEntities';
import { Information } from '../information/information';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class Register {

  driverUser = new DriverEntity();
  driverInfo: Object;
  // informationPage: 'Information';

  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider: RestProvider) {
    var emptyField = "";
    this.driverUser.firstName = emptyField;
    this.driverUser.lastName = emptyField;
    this.driverUser.password = emptyField;
    this.driverUser.phone = null;

  }

// back function
  backButtonClick(){
    this.navCtrl.pop();
  }

// intlTelInput for select country id
  ngOnInit(): any {
    let telInput = $("#phone");

    telInput.intlTelInput();
    // listen to "keyup", but also "change" to update when the user selects a country
    telInput.on("keyup change", function() {
      // var intlNumber = telInput.intlTelInput("getNumber");
    });
  }

  saveDriverCredentials(){
    
    this.driverInfo = {
      "Prénoms": this.driverUser.firstName, 
      "Nom": this.driverUser.lastName,
      "MotDePasse" : this.driverUser.password,
      "Phone" : this.driverUser.phone
    };
    // console.log(this.driverInfo);
    this.restProvider.postDriverInfo(this.driverInfo);
    this.navCtrl.push(Information);
    
  }
}