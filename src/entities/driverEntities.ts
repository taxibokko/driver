export class DriverEntity{

    // public static driverSingleton: DriverEntity;
    
    constructor(private _firstName?: string, private _lastName?: string,  private _phone?: number, private _password?: string){
        
    }

    set firstName(firstNameGiven: string){
        this._firstName = firstNameGiven;
    }
    
    set lastName(lastNameGiven: string){
        this._lastName = lastNameGiven;
    }

    set phone(phoneGiven: number){
        this._phone = phoneGiven;
    }

    set password (passwordGiven:string){
        this._password = passwordGiven;
    }

    get firstName (): string{
        return this._firstName;
    }
    
    get lastName(): string {
        return this._lastName;
    }

    get phone(): number {
        return this._phone;
    }

    get password (): string {
        return this._password;
    }


}
export class DriverLicenseEntity{
    driverLicenceNumber: string;
    vehiculeType: string;
    dateIssuedOn: Date;
    dateExpiration: Date;
    constructor(){

    }
}